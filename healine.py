from googlesearch import search
from bs4 import BeautifulSoup
import requests
import csv
from unicodedata import normalize
import hashlib
from collections import Counter
import click

finalList = []

hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
         'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
         'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
         'Accept-Encoding': 'none',
         'Accept-Language': 'en-US,en;q=0.8',
         'Connection': 'keep-alive'}

numTable = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
restrictedWords = [
    "days",
    "local",
    "best",
    "things to do",
    "reviews",
    "weather",
    "save",
    "this",
    "sponsored",
    "you",
    "attractions",
    "provided",
    "get",
    "activities",
    "guide",
    "comments",
    "about",
    "related",
    "home",
    "search",
    "start",
    "welcome",
    "top",
    "tips",
    "out",
    "work",
    "saving",
    "free",
    "us",
    "blog",
    "compare",
    "sex"
]

# def add_text_to_final_list(string, type: str):
#     sText:str = string.get_text()
#     for n in numTable:
#         if sText.startswith(str(i)) and sText and not any(words in sText.lower() for words in restrictedWords):
#             print(sText.strip() + type)
#             normal_s_text = str(normalize('NFKD', sText.strip()))
#             finalList.append(normal_s_text.lower())

@click.command()
@click.option('--num-pages', default=5, help='Number of pages on google it will crawl.')
@click.option('--place', prompt='Where do you want to travel to?', help='Where you want to travel to')
@click.option('--verbose', "-v", help='Do you want your terminal to get spammed?')
def doSearch(num_pages, place, verbose):
    for i in search("things to do in" + place, stop=1, num=num_pages, safe="on", lang="en"):

        print("==================================================================")
        print(i)
        print("==================================================================")
        html = requests.get(i).content
        soup = BeautifulSoup(html, "lxml")

        for i in range(1, 4):
            for h in soup.find_all("h" + str(i)):
                hText:str = h.get_text()
                if hText and not any(words in hText.lower() for words in restrictedWords):
                    if verbose:
                        print(hText.strip() + ":h")
                    normal_h_text = str(normalize('NFKD', hText.strip()))
                    finalList.append(normal_h_text.lower())
        for p in soup.find_all("p"):
            # add_text_to_final_list(p)
            pText:str = p.get_text()
            for n in numTable:
                if pText.startswith(str(n)) and pText and not any(words in pText.lower() for words in restrictedWords):
                    if verbose:
                        print(pText.strip() + ":p")
                    normal_p_text = str(normalize('NFKD', pText.strip()))
                    finalList.append(normal_p_text.lower())
        for s in soup.find_all("strong"):
            # add_text_to_final_list(s)
            sText:str = s.get_text()
            for n in numTable:
                if sText.startswith(str(n)) and sText and not any(words in sText.lower() for words in restrictedWords):
                    if verbose:
                        print(sText.strip() + ":p")
                    normal_s_text = str(normalize('NFKD', sText.strip()))
                    finalList.append(normal_s_text.lower())
        for a in soup.find_all("a", attrs={"class": "poiTitle"}):
            aText: str = a.get_text()
            if aText and not any(words in aText.lower() for words in restrictedWords):
                if verbose:
                    print(aText.strip() + ":a")
                normal_a_text = str(normalize('NFKD', aText.strip()))
                finalList.append(normal_a_text.lower())
    if verbose:
        print(finalList)
        print(len(finalList))
        print(Counter(finalList))

    fl_withRanks = dict(Counter(finalList))

    csvName = hashlib.sha256(''.join(str(x) for x in finalList).encode("utf-8")).hexdigest()

    def writeToCSV():
        with open(csvName + '.csv', 'w') as csvfile:
            fieldnames = ['place_name', 'rank']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for k, v in fl_withRanks.items():
                writer.writerow({'place_name': str(k.encode("utf-8"))[2:-1], 'rank': v})

    writeToCSV()

    print("Saved to " + csvName + ".csv")

if __name__ == '__main__':
    doSearch()

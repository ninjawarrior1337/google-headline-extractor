import requests as requests
from googlesearch import search
from bs4 import BeautifulSoup
import requests
import csv
from unicodedata import normalize
import hashlib
from collections import Counter

hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
         'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
         'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
         'Accept-Encoding': 'none',
         'Accept-Language': 'en-US,en;q=0.8',
         'Connection': 'keep-alive'}

url = "https://www.tripadvisor.com/Attractions-g60763-Activities-New_York_City_New_York.html"

html = requests.get(url).content
soup = BeautifulSoup(html, "lxml")

print(soup.prettify().encode("utf-8"))

for h in soup.find_all("a", attrs={"class": "poiTitle"}):
    hText: str = h.get_text()
    print(hText.strip() + ":h")

